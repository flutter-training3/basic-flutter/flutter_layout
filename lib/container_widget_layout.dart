import 'package:flutter/material.dart';

class ContainerWidgetLayout extends StatefulWidget {
  const ContainerWidgetLayout({Key? key}) : super(key: key);

  @override
  State<ContainerWidgetLayout> createState() => _ContainerWidgetLayoutState();
}

class _ContainerWidgetLayoutState extends State<ContainerWidgetLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Container Layout'),
      ),
      body: Center(
        child: Container(
          // color: const Color.fromARGB(200, 23, 123, 222),
          // color: const Color.fromRGBO(200, 23, 123, 0.75),
          // color: const Color(0xFFcde232),
          // color: Colors.green,
          // color: Colors.green[200],
          // color: Colors.green.shade300,
          width: 300,
          height: 300,
          decoration: BoxDecoration(
              color: const Color(0xFF00e333),
              border: Border.all(width: 5, color: Colors.brown),
              // borderRadius: BorderRadius.circular(20),
              shape: BoxShape.circle,
              image: const DecorationImage(
                  image: NetworkImage('https://placekitten.com/g/300/300'),
                  fit: BoxFit.cover),
              boxShadow: [BoxShadow(blurRadius: 20)]),
          // child: const Text('Content'),
        ),
      ),
    );
  }
}
