import 'package:flutter/material.dart';
import 'package:flutter_layout/home_screen.dart';

class Welcome extends StatefulWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  State<Welcome> createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Title'),
      // ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.blue, Colors.teal],
                tileMode: TileMode.repeated)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildTextHeader(),
            _buildTextWelcome(),
            _buildButtonStart()
          ],
        ),
      ),
    );
  }

  // Header Widget
  Widget _buildTextHeader() {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            const Icon(
              Icons.help,
              color: Colors.white,
            ),
            TextButton(
                onPressed: () {},
                child: const Text(
                  'ช่วยเหลือ',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )),
            const SizedBox(
              width: 10,
            ),
            TextButton(
                onPressed: () {},
                child: const Text(
                  'ภาษาไทย',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )),
          ],
        ),
      ),
    );
  }

  // Welcome Widget
  Widget _buildTextWelcome() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text(
          'สวัสดี',
          style: TextStyle(
              fontSize: 36, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        Text(
          'ยินดีต้อนรับสู่บริการโมบายแบงก์กิ้ง',
          style: TextStyle(
            fontSize: 24,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  // Button Start Widget
  Widget _buildButtonStart() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      width: double.infinity,
      child: ElevatedButton(
          onPressed: () {
            // การทำ Navigator route แบบ push
            // Navigator.push(context,
            //     MaterialPageRoute(builder: (context) => const HomeScreen()));

            // การทำ Navigation แบบ pushReplacement
            // Navigator.pushReplacement(context,
            //     MaterialPageRoute(builder: (context) => const HomeScreen()));

            // การทำ Navigation แบบ pushNamed
            // Navigator.pushNamed(context, '/home_screen');

            // การทำ Navigation แบบ pushReplacementNamed
            Navigator.pushReplacementNamed(context, '/home_screen');
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.white,
              foregroundColor: Colors.blue,
              padding: const EdgeInsets.all(10)),
          child: const Text(
            'เริ่มเต้นใช้งาน',
            style: TextStyle(
              fontSize: 24,
            ),
          )),
    );
  }
}
