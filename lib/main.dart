import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_layout/home_screen.dart';
// import 'package:flutter_layout/expanded_widget.dart';
import 'package:flutter_layout/welcome.dart';
// import 'package:flutter_layout/container_widget_layout.dart';
// import 'package:flutter_layout/row_column_widget.dart';
// import 'package:flutter_layout/stack_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // คำสั่งตั้งค่า lock screen เป็นแนวตั้ง
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      title: 'Flutter Layout',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'Chakra Petch'),
      routes: {
        '/welcome': (context) => const Welcome(),
        '/home_screen': (context) => const HomeScreen()
      },
      initialRoute: '/welcome',
      // home: const Welcome(),
    );
  }
}
