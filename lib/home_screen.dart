import 'package:flutter/material.dart';
import 'package:flutter_layout/welcome.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Home Screen',
              style: TextStyle(fontSize: 30),
            ),
            ElevatedButton(
                onPressed: () => Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => const Welcome())),
                child: const Text('Back to welcome with MaterialPageRoute')),
            ElevatedButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('Back to welcome with pop'))
          ],
        ),
      ),
    );
  }
}
