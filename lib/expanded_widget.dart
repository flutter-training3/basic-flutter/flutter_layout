import 'package:flutter/material.dart';

class ExpandedWidget extends StatefulWidget {
  const ExpandedWidget({Key? key}) : super(key: key);

  @override
  State<ExpandedWidget> createState() => _ExpandedWidgetState();
}

class _ExpandedWidgetState extends State<ExpandedWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Expaned Widget'),
      ),
      body: Column(
        children: [
          Container(
            height: 100,
            color: Colors.green,
          ),
          Expanded(
              flex: 4,
              child: Container(
                color: Colors.yellow,
              )),
          Expanded(
              flex: 1,
              child: Container(
                color: Colors.blue,
              ))
        ],
      ),
    );
  }
}
